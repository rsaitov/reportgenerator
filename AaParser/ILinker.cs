﻿using AaParser.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Linker
{
    public class LinkerFactory
    {
        public static ILinker CreateLinker(string fileName)
        {
            using (var reader = new StreamReader(fileName))
            {
                string line = "";
                line = reader.ReadLine();
                var headers = line.Replace("\"", "").Split('\t').ToList();
                headers.ForEach(x => x = x.ToUpperInvariant());

                if(headers.Contains("AccountNumber"))
                    return new AALinker();

                if (headers.Contains("код клиента"))
                    return new lCLinker();                
            }

            return null;
        }
    }

    public interface ILinker
    {
        void LoadSaldo(string file);
        string GetSaldo(ActiveContractsFromAaRow row);
    }

    public class AALinker : ILinker
    {
        private List<SaldoFromAaRow> _rowsFromAa { get; set; }

        public string GetSaldo(ActiveContractsFromAaRow row)
        {
            var saldo = _rowsFromAa.FirstOrDefault(x => x.AccountNumber == row.AccountNumber);
            return (saldo != null && saldo.Saldo != null) ? saldo.Saldo : "0";
        }

        public void LoadSaldo(string file)
        {
            _rowsFromAa = new List<SaldoFromAaRow>();
            using (var reader = new StreamReader(file))
            {
                List<string> headers = null;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Replace("\"", "").Split('\t');

                    if (ReferenceEquals(headers, null))
                    {
                        headers = new List<string>(values);
                        continue;
                    }

                    var activeContractRow = new SaldoFromAaRow();
                    for (int i = 0; i < values.Length; i++)
                        switch (headers[i].ToString().Trim().ToUpperInvariant())
                        {
                            case @"ACCOUNTNUMBER":
                                activeContractRow.AccountNumber = values[i];
                                break;
                            case @"SALDO":
                                activeContractRow.Saldo = values[i];
                                break;
                        }

                    _rowsFromAa.Add(activeContractRow);
                }
            }
        }
    }

    public class lCLinker : ILinker
    {
        private List<SaldoFromAaRow> _rowsFromAa { get; set; }
        public string GetSaldo(ActiveContractsFromAaRow row)
        {
            var saldo = _rowsFromAa.FirstOrDefault(x => x.AccountNumber == row.НомерАнкеты);
            return (saldo != null && saldo.Saldo != null) ? saldo.Saldo : "0";
        }

        public void LoadSaldo(string file)
        {
            _rowsFromAa = new List<SaldoFromAaRow>();
            using (var reader = new StreamReader(file))
            {
                List<string> headers = null;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Replace("\"", "").Split('\t');

                    if (ReferenceEquals(headers, null))
                    {
                        headers = new List<string>(values);
                        continue;
                    }

                    var activeContractRow = new SaldoFromAaRow();
                    for (int i = 0; i < values.Length; i++)
                        switch (headers[i].ToString().Trim().ToUpperInvariant())
                        {
                            case @"КОД КЛИЕНТА":
                                activeContractRow.AccountNumber = values[i];
                                break;
                            case @"ОСТАТОК НА ДЕПОЗИТЕ":
                                activeContractRow.Saldo = values[i];
                                break;
                        }

                    _rowsFromAa.Add(activeContractRow);
                }
            }
        }
    }
}
