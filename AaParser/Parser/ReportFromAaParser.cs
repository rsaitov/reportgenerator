﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AaParser.Entity;
using Excel;

namespace AaParser.Parser
{
    public static class ReportFromAaParser
    {
        private const string EmptyFile = @"В файле не найдены таблицы с данными (пустой файл)";

        public static List<BlocksLeftFromAaRow> ParseBlocksLeftFromAa(string fileName)
        {
            var rowsFromAa = new List<BlocksLeftFromAaRow>();
            using (var reader = new StreamReader(fileName))
            {
                List<string> headers = null;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Replace("\"", "").Split('\t');

                    if (ReferenceEquals(headers, null))
                    {
                        headers = new List<string>(values);
                        continue;
                    }

                    var blocksLeftRow = new BlocksLeftFromAaRow();
                    for (int i = 0; i < values.Length; i++)
                        switch (headers[i].ToString().Trim().ToUpperInvariant())
                        {
                            case @"GROUPNAME":
                                blocksLeftRow.GroupName = values[i];
                                break;
                            case @"CTYPE":
                                blocksLeftRow.CType = values[i];
                                break;
                            case @"STATUS":
                                blocksLeftRow.Status = values[i];
                                break;
                            case @"FIO":
                                blocksLeftRow.Fio = values[i];
                                break;
                            case @"CARD":
                                blocksLeftRow.Card = values[i];
                                break;
                            case @"STDATA":
                                blocksLeftRow.StData = values[i];
                                break;
                            case @"SPDATA":
                                blocksLeftRow.SpData = values[i];
                                break;
                            case @"INFO":
                                blocksLeftRow.Info = values[i];
                                break;
                            case @"ID":
                                blocksLeftRow.Id = values[i];
                                break;
                            case @"SERVICEID":
                                blocksLeftRow.ServiceId = values[i];
                                break;
                            case @"SNAME":
                                blocksLeftRow.Sname = values[i];
                                break;
                            case @"ITOG":
                                blocksLeftRow.Itog = values[i];
                                break;
                            case @"NPERIOD":
                                blocksLeftRow.NPeriod = values[i];
                                break;
                            case @"CREATED":
                                blocksLeftRow.Created = values[i];

                                var temp = DateTime.MinValue;
                                var result = DateTime.TryParse(values[i], out temp);
                                if (result)
                                    blocksLeftRow.CreatedDt = temp;
                                break;
                            case @"CREATEDBY":
                                blocksLeftRow.CreatedBy = values[i];
                                break;
                            case @"GROUPID":
                                blocksLeftRow.GroupId = values[i];
                                break;
                            case @"EXPIREDDATE":
                                blocksLeftRow.ExpiredDate = values[i];
                                break;
                            case @"TOTALQUANTITY":
                                blocksLeftRow.TotalQuantity = values[i];
                                break;
                            case @"TOTALDONE":
                                blocksLeftRow.TotalDone = values[i];
                                break;
                            case @"RESOURCE":
                                blocksLeftRow.Resource = values[i];
                                break;
                            case @"PERSONNUMBER":
                                blocksLeftRow.PersonNumber = values[i];
                                break;
                        }

                    rowsFromAa.Add(blocksLeftRow);
                }
            }

            return rowsFromAa;
        }
        public static List<SalesFromAaRow> ParseSalesFromAa(string fileName)
        {
            var rowsFromAa = new List<SalesFromAaRow>();
            using (var reader = new StreamReader(fileName))
            {
                List<string> headers = null;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Replace("\"", "").Split('\t');

                    if (ReferenceEquals(headers, null))
                    {
                        headers = new List<string>(values);
                        continue;
                    }

                    var salesRow = new SalesFromAaRow();
                    for (int i = 0; i < values.Length; i++)
                        switch (headers[i].ToString().Trim().ToUpperInvariant())
                        {
                            case @"DOCN":
                                salesRow.Docn = values[i];
                                break;
                            case @"SRVNAME":
                                salesRow.SrvCode = values[i].Substring(values[i].IndexOf('(') + 1, values[i].IndexOf(')') - values[i].IndexOf('(') - 1).Trim();
                                salesRow.SrvName = values[i].Substring(values[i].IndexOf(')') + 1).Trim();
                                break;
                            case @"QUANTITY":
                                salesRow.Quantity = values[i];
                                break;
                            case @"SDATA":
                                salesRow.SData = values[i];
                                break;
                            case @"PRICE":
                                salesRow.Price = values[i];
                                break;
                            case @"DISCOUNT":
                                salesRow.Discount = values[i];
                                break;
                            case @"RNAME":
                                salesRow.Rname = values[i];
                                break;
                            case @"OPERATOR":
                                salesRow.Operator = values[i];
                                break;
                            case @"GRPNAME":
                                salesRow.GrpName = values[i];
                                break;
                            case @"FIO":
                                salesRow.Fio = values[i];
                                salesRow.PersonNumber = ExtractPersonNumberFromFio(values[i]);
                                break;
                            case @"PDATA":
                                salesRow.Pdata = values[i];

                                var temp = DateTime.MinValue;
                                var result = DateTime.TryParse(values[i], out temp);
                                if (result)
                                    salesRow.PDataDt = temp;
                                break;
                            case @"SUMMA":
                                salesRow.Summa = values[i];
                                break;
                            case @"RECORDID":
                                salesRow.RecordId = values[i];
                                break;
                            case @"PCODE":
                                salesRow.Pcode = values[i];
                                break;
                            case @"PDATAG":
                                salesRow.PdataG = values[i];
                                break;
                            case @"PAYTYPE":
                                salesRow.PayType = values[i];
                                break;
                            case @"PTYPE":
                                salesRow.PayType = values[i];
                                break;
                            case @"COMMENT":
                                salesRow.Comment = values[i];
                                break;
                            case @"TOTALPRICE":
                                salesRow.TotalPrice = values[i];
                                break;
                        }

                    rowsFromAa.Add(salesRow);
                }
            }

            return rowsFromAa;
        }
        public static List<PhonesFromProfileRow> ParsePhonesFromProfile(string fileName)
        {
            var stream = File.Open(fileName, FileMode.Open, FileAccess.Read);
            var excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            var result = excelReader.AsDataSet();

            ThrowIfNoTablesFound(result, fileName);

            var dtImportSheet = result.Tables[0];

            var list = new List<PhonesFromProfileRow>();
            for (int row = 0; row < dtImportSheet.Rows.Count; row++)
            {
                var item = new PhonesFromProfileRow();

                item.PersonNumber = dtImportSheet.Rows[row][0].ToString();
                item.Phone = dtImportSheet.Rows[row][1].ToString();

                list.Add(item);
            }

            return list;
        }
        private static void ThrowIfNoTablesFound(DataSet result, string fileName)
        {
            if (result.Tables.Count == 0)
                throw new Exception(EmptyFile + ": " + fileName);
        }
        public static List<RealizationFromAaRow> ParseRealizationFromAa(string fileName)
        {
            var rowsFromAa = new List<RealizationFromAaRow>();
            using (var reader = new StreamReader(fileName))
            {
                List<string> headers = null;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Replace("\"", "").Split('\t');

                    if (ReferenceEquals(headers, null))
                    {
                        headers = new List<string>(values);
                        continue;
                    }

                    var row = new RealizationFromAaRow();
                    for (int i = 0; i < values.Length; i++)
                        if (i < headers.Count)
                            switch (headers[i].ToString().Trim().ToUpperInvariant())
                            {
                                case @"GRPLEVEL1":
                                    row.GrpLevel1 = values[i].Trim();
                                    break;
                                case @"LEVELNAME1":
                                    row.LevelName1 = values[i].Trim();
                                    break;
                                case @"GRPLEVEL2":
                                    row.GrpLevel2 = values[i].Trim();
                                    break;
                                case @"LEVELNAME2":
                                    row.LevelName2 = values[i].Trim();
                                    break;
                                case @"FIO":
                                    row.Fio = values[i].Trim();
                                    row.PersonNumber = ExtractPersonNumberFromFio(values[i]);
                                    break;
                                case @"DAT":
                                    row.Dat = values[i].Trim();

                                    var temp = DateTime.MinValue;
                                    var result = DateTime.TryParse(values[i], out temp);
                                    if (result)
                                        row.DatDt = temp;
                                    break;
                                case @"PRICE":
                                    row.Price = values[i].Trim();
                                    break;
                                case @"QUANT":
                                    row.Quant = values[i].Trim();
                                    break;
                                case @"SUMMA":
                                    row.Summa = values[i].Trim();
                                    break;
                                case @"NAME":
                                    row.Name = values[i].Trim();
                                    break;
                                case @"DETAILCLIENT":
                                    row.DetailClient = values[i].Trim();
                                    break;
                                case @"QUANTFREE":
                                    row.QuantFree = values[i].Trim();
                                    break;
                                case @"TIMEMES":
                                    row.TimeMes = values[i].Trim();
                                    break;
                                case @"DETAILDATE":
                                    row.DetailDate = values[i].Trim();
                                    break;
                                case @"PAYTYPE":
                                    row.PayType = values[i].Trim();
                                    break;
                                case @"SERVICECODE":
                                    row.ServiceCode = values[i].Trim();
                                    break;
                                case @"RECOURCEID":
                                    row.ResourceId = values[i].Trim();
                                    break;
                            }

                    rowsFromAa.Add(row);
                }
            }

            return rowsFromAa;
        }
        private static string ExtractPersonNumberFromFio(string s)
        {
            return s.Substring(s.IndexOf('(') + 1, s.IndexOf(')') - s.IndexOf('(') - 1).Trim();
        }
        public static List<ActiveContractsFromAaRow> ParseActiveContracts(string fileName)
        {
            var rowsFromAa = new List<ActiveContractsFromAaRow>();
            using (var reader = new StreamReader(fileName))
            {
                List<string> headers = null;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Replace("\"", "").Split('\t');

                    if (ReferenceEquals(headers, null))
                    {
                        headers = new List<string>(values);
                        continue;
                    }

                    var activeContractRow = new ActiveContractsFromAaRow();
                    for (int i = 0; i < values.Length; i++)
                        switch (headers[i].ToString().Trim().ToUpperInvariant())
                        {
                            case @"НОМЕР АНКЕТЫ":
                            case @"КОД КЛИЕНТА":
                                activeContractRow.НомерАнкеты = values[i];
                                break;
                            case @"ФАМИЛИЯ":
                            case @"ФИО КЛИЕНТА":
                                activeContractRow.Фамилия = values[i];
                                break;
                            case @"ИМЯ":
                                activeContractRow.Имя = values[i];
                                break;
                            case @"ОТЧЕСТВО":
                                activeContractRow.Отчество = values[i];
                                break;
                            case @"TEMPLATENAME":
                            case @"ЧЛЕНСТВО":
                                activeContractRow.TemplateName = values[i];
                                break;
                            case @"DATEFROM":
                            case @"ДАТА АКТИВАИИ":
                                activeContractRow.DateFrom = values[i];
                                break;
                            case @"DATETOWFREEZE":
                            case @"ДЕЙСТВУЕТ ДО":
                                activeContractRow.DateToWFreeze = values[i];
                                break;
                            case @"CARD":
                                activeContractRow.Card = values[i];
                                break;
                            case @"STATUS":
                                activeContractRow.Status = values[i];
                                break;
                            case @"ACCOUNTNUMBER":
                                activeContractRow.AccountNumber = values[i];
                                break;
                            //case @"CREATED":
                            //    activeContractRow.Created = values[i];

                            //    var temp = DateTime.MinValue;
                            //    var result = DateTime.TryParse(values[i], out temp);
                            //    if (result)
                            //        activeContractRow.CreatedDt = temp;
                            //    break;
                        }

                    rowsFromAa.Add(activeContractRow);
                }
            }

            return rowsFromAa;
        }
        public static List<VisitsFromAaRow> ParseVisits(string fileName)
        {
            var rowsFromAa = new List<VisitsFromAaRow>();
            using (var reader = new StreamReader(fileName))
            {
                List<string> headers = null;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Replace("\"", "").Split('\t');

                    if (ReferenceEquals(headers, null))
                    {
                        headers = new List<string>(values);
                        continue;
                    }

                    var activeContractRow = new VisitsFromAaRow();
                    for (int i = 0; i < values.Length; i++)
                        switch (headers[i].ToString().Trim().ToUpperInvariant())
                        {
                            case @"НОМЕР АНКЕТЫ":
                            case @"КЛИЕНТ.КОД":
                                activeContractRow.НомерАнкеты = values[i];
                                break;
                            case @"ENTERTIME":
                            case @"ПОСЕЩЕНИЕ.ДАТА ПРИХОДА":                                
                                activeContractRow.EnterTime = values[i];

                                var tempEnter = DateTime.MinValue;
                                var resultEnter = DateTime.TryParse(values[i], out tempEnter);
                                if (resultEnter)
                                    activeContractRow.EnterTimeDateTime = tempEnter;

                                break;
                            case @"EXITTIME":
                            case @"ПОСЕЩЕНИЕ.ДАТА УХОДА":
                                activeContractRow.ExitTime = values[i];

                                var tempExit = DateTime.MinValue;
                                var resultExit = DateTime.TryParse(values[i], out tempExit);
                                if (resultExit)
                                    activeContractRow.ExitTimeDateTime = tempExit;

                                break;
                        }

                    rowsFromAa.Add(activeContractRow);
                }
            }

            return rowsFromAa;
        }

        public static List<SeansesFromAaRow> ParseSeanses(string fileName)
        {
            var rowsFromAa = new List<SeansesFromAaRow>();
            using (var reader = new StreamReader(fileName))
            {
                List<string> headers = null;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Replace("\"", "").Split('\t');

                    if (ReferenceEquals(headers, null))
                    {
                        headers = new List<string>(values);
                        continue;
                    }

                    var activeContractRow = new SeansesFromAaRow();
                    for (int i = 0; i < values.Length; i++)
                        switch (headers[i].ToString().Trim().ToUpperInvariant())
                        {
                            case @"НОМЕР АНКЕТЫ":
                            case @"КОД КЛИЕНТА":
                            case @"КЛИЕНТ.КОД":
                                activeContractRow.НомерАнкеты = values[i];
                                break;
                            case @"SECTIONNAME":
                            case @"УСЛУГА, СЕГМЕНТ.ПОДРАЗДЕЛЕНИЕ":
                                activeContractRow.SectionName = values[i];
                                break;
                            case @"NUMDONE":
                                activeContractRow.NumDone = values[i];
                                break;
                            case @"NUMTOTAL":
                                activeContractRow.NumTotal = values[i];
                                break;
                            case @"EXECUTED":
                            case @"ЗАНЯТИЕ.ДАТА":
                                activeContractRow.Executed = values[i];

                                var tempExecuted = DateTime.MinValue;
                                var resultExit = DateTime.TryParse(values[i], out tempExecuted);
                                if (resultExit)
                                    activeContractRow.ExecutedDateTime = tempExecuted;

                                break;
                            case @"EXECUTOR":
                            case @"СОТРУДНИК":
                                activeContractRow.Executor = values[i];
                                break;
                            case @"SERVICENAME":
                            case @"НОМЕНКЛАТУРА":
                                activeContractRow.ServiceName = values[i];
                                break;
                        }

                    rowsFromAa.Add(activeContractRow);
                }
            }

            return rowsFromAa;
        }

        public static List<SaldoFromAaRow> ParseSaldo(string fileName)
        {
            var rowsFromAa = new List<SaldoFromAaRow>();
            using (var reader = new StreamReader(fileName))
            {
                List<string> headers = null;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Replace("\"", "").Split('\t');

                    if (ReferenceEquals(headers, null))
                    {
                        headers = new List<string>(values);
                        continue;
                    }

                    var activeContractRow = new SaldoFromAaRow();
                    for (int i = 0; i < values.Length; i++)
                        switch (headers[i].ToString().Trim().ToUpperInvariant())
                        {
                            case @"ACCOUNTNUMBER":
                                activeContractRow.AccountNumber = values[i];
                                break;
                            case @"SALDO":
                                activeContractRow.Saldo = values[i];
                                break;
                        }

                    rowsFromAa.Add(activeContractRow);
                }
            }

            return rowsFromAa;
        }
    }
}
