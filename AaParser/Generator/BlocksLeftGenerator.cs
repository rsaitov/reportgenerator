﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AaParser.Entity;

namespace AaParser.Generator
{
    public static class BlocksLeftGenerator
    {
        public static List<BlocksLeftRow> Generate(List<BlocksLeftFromAaRow> blocksLeftFromAa, List<SalesFromAaRow> salesFromAa, List<PhonesFromProfileRow> phonesFromProfile)
        {
            var startDate = new DateTime(2015, 01, 01);

            var list = new List<BlocksLeftRow>();
            var blocksLeftFromAaFiltered = blocksLeftFromAa.Where(x => !x.GroupName.ToUpperInvariant().Contains(@"БЕСПЛАТНЫЕ") &&
                                                                       DateTime.Parse(x.Created.Substring(0, 10)) > startDate)
                                                                       .OrderBy(x => x.CreatedDt);

            foreach (var row in blocksLeftFromAaFiltered)
            {
                var blocksLeftRow = new BlocksLeftRow();
                var personNumber = row.PersonNumber.Trim();

                var salesRow = salesFromAa.FirstOrDefault(x => string.Equals(x.PersonNumber.Trim(), personNumber)
                                                               && string.Equals(x.SData.Substring(0, 10), row.Created.Substring(0, 10))
                                                               && string.Equals(x.SrvName, row.Sname));

                var phones = string.Join(" / ", phonesFromProfile.Where(x => string.Equals(x.PersonNumber.Trim(), personNumber)).Select(x => x.Phone));

                blocksLeftRow.Фио = row.Fio;
                blocksLeftRow.Шаблон= row.Info;
                blocksLeftRow.Услуга = row.Sname;
                blocksLeftRow.Исполнитель = row.Resource;
                blocksLeftRow.Остаток = row.Itog;
                blocksLeftRow.Создан = row.Created.Substring(0, 10);
                blocksLeftRow.Чек = ReferenceEquals(salesRow, null) ? "" : salesRow.Docn;
                blocksLeftRow.ТипОплаты = ReferenceEquals(salesRow, null) ? "" : salesRow.Pcode;
                blocksLeftRow.Телефоны = phones;

                list.Add(blocksLeftRow);
            }

            return list;
        }

        public static List<List<string>> ToListList(List<BlocksLeftRow> blocksLeft)
        {
            var list = new List<List<string>>();
            var headerRow = new List<string>();
            headerRow.Add("Фио");
            headerRow.Add("Шаблон");
            headerRow.Add("Услуга");
            headerRow.Add("Исполнитель");
            headerRow.Add("Остаток");
            headerRow.Add("Создан");
            headerRow.Add("Чек");
            headerRow.Add("ТипОплаты");
            headerRow.Add("Телефоны");
            list.Add(headerRow);

            foreach (var row in blocksLeft)
            {
                var blocksLeftRow = new List<string>();

                blocksLeftRow.Add(row.Фио);
                blocksLeftRow.Add(row.Шаблон);
                blocksLeftRow.Add(row.Услуга);
                blocksLeftRow.Add(row.Исполнитель);
                blocksLeftRow.Add(row.Остаток);
                blocksLeftRow.Add(row.Создан);
                blocksLeftRow.Add(row.Чек);
                blocksLeftRow.Add(row.ТипОплаты);
                blocksLeftRow.Add(row.Телефоны);

                list.Add(blocksLeftRow);
            }

            return list;
        }
    }
}
