﻿using AaParser.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AaParser.Linker;

namespace AaParser.Generator
{
    public enum VisitsAndSeansesReportType { ALL, BASE, DEP_UNDER_3000, DEP_OVER_3000, MEDIAN, NOFILTER }
    public static class VisitsAndSeansesGenerator
    {
        public static string typeDepFilter = "ДЕП";
        
        public static List<VisitsAndSeansesRow> Generate(List<ActiveContractsFromAaRow> activeContractsFromAa, List<VisitsFromAaRow> visitsFromAa,
            List<SeansesFromAaRow> seansesFromAa, ILinker saldoLinker, VisitsAndSeansesReportType type)
        //List<SeansesFromAaRow> seansesFromAa, List<SaldoFromAaRow> saldoFromAa, VisitsAndSeansesReportType type)            
        {
            var list = new List<VisitsAndSeansesRow>();

            switch(type)
            {                
                case VisitsAndSeansesReportType.BASE:
                    activeContractsFromAa = activeContractsFromAa.Where(x => !x.TemplateName.ToUpperInvariant().Contains(typeDepFilter)).ToList();
                    break;
                case VisitsAndSeansesReportType.DEP_UNDER_3000:
                case VisitsAndSeansesReportType.DEP_OVER_3000:
                    activeContractsFromAa = activeContractsFromAa.Where(x => x.TemplateName.ToUpperInvariant().Contains(typeDepFilter)).ToList();
                    break;
                case VisitsAndSeansesReportType.ALL:
                case VisitsAndSeansesReportType.NOFILTER:
                default:
                    break;
            }

            if (type != VisitsAndSeansesReportType.NOFILTER)
                seansesFromAa = seansesFromAa.Where(x => !x.ServiceName.ToUpperInvariant().Contains("ДЦ") && 
                    !x.ServiceName.ToUpperInvariant().Contains("ДЕТСКИЙ ЦЕНТР")).ToList();

            var totalRow = new VisitsAndSeansesRow();
            totalRow.ФИО = "Итого";
            totalRow.Посещения = "0";
            totalRow.Занятия = "0";            

            foreach (var row in activeContractsFromAa)
            {
                if (row.TemplateName.ToUpperInvariant().EndsWith("СОТРУДНИК") ||
                    row.TemplateName.ToUpperInvariant().EndsWith("СОТРУДНИКИ"))
                    continue;

                var tempList = new List<VisitsAndSeansesRow>();

                var visitAndSeansesRow = new VisitsAndSeansesRow();
                var personNumber = row.НомерАнкеты.Trim();
                var visitsOfContract = visitsFromAa.Where(x => x.НомерАнкеты == personNumber && (x.ExitTimeDateTime - x.EnterTimeDateTime).TotalMinutes > 60);
                if (visitsOfContract.Count() == 0) 
                    continue;

                visitAndSeansesRow.НомерАнкеты = personNumber;
                visitAndSeansesRow.ФИО = String.Format($"{row.Фамилия} {row.Имя} {row.Отчество}"); 
                visitAndSeansesRow.Шаблон = row.TemplateName;                
                visitAndSeansesRow.Посещения = visitsOfContract.Count().ToString();

                var seanses = seansesFromAa.Where(x => x.НомерАнкеты == personNumber && 
                    (x.NumTotal == null || Int32.Parse(x.NumTotal) != 1)).OrderBy(x => x.ExecutedDateTime).ToList();
                if (type != VisitsAndSeansesReportType.NOFILTER && seanses.Count() == 0)
                    continue;
                
                if (type != VisitsAndSeansesReportType.MEDIAN && type != VisitsAndSeansesReportType.NOFILTER && visitsOfContract.Count() / seanses.Count() <= 3)
                    continue;

                visitAndSeansesRow.ОстатокЛС = saldoLinker.GetSaldo(row);

                if (type == VisitsAndSeansesReportType.DEP_UNDER_3000 && decimal.Parse(visitAndSeansesRow.ОстатокЛС) > 3000)
                    continue;
                if (type == VisitsAndSeansesReportType.DEP_OVER_3000 && decimal.Parse(visitAndSeansesRow.ОстатокЛС) <= 3000)
                    continue;

                visitAndSeansesRow.Занятия = seanses.Count().ToString();

                foreach (var visit in visitsOfContract)
                {
                    var newRow = new VisitsAndSeansesRow();
                    newRow.Приход = visit.EnterTime;
                    newRow.Уход = visit.ExitTime;

                    var seansesThisVisit = seanses.Where(x => x.ExecutedDateTime >= visit.EnterTimeDateTime && 
                        x.ExecutedDateTime <= visit.ExitTimeDateTime).ToList();

                    if (seansesThisVisit != null && seansesThisVisit.Count() > 0)
                    {
                        foreach (var seanse in seansesThisVisit)
                        {
                            if (type != VisitsAndSeansesReportType.NOFILTER && seanse.NumTotal != null &&
                                Int32.Parse(seanse.NumTotal) == 1 && Int32.Parse(seanse.NumDone) == 1)
                            {
                                visitAndSeansesRow.Посещения = (Int32.Parse(visitAndSeansesRow.Посещения) - 1).ToString();
                                visitAndSeansesRow.Занятия = (Int32.Parse(visitAndSeansesRow.Занятия) - 1).ToString();
                                seanses.Remove(seanse);
                                continue;
                            }

                            var seanseRow = (VisitsAndSeansesRow)newRow.Clone();
                            seanseRow.Тренер = seanse.Executor;
                            seanseRow.Подразделение = seanse.SectionName;
                            seanseRow.Занятие = seanse.Executed;
                            seanseRow.Блок = seanse.ServiceName;
                            seanseRow.Использовано = seanse.NumDone ?? "";
                            seanseRow.ВсегоВблоке = seanse.NumTotal ?? "";
                            tempList.Add(seanseRow);

                            seanses.Remove(seanse);
                        }
                    }
                    else
                        tempList.Add(newRow);
                }

                if (seanses.Count() > 0)
                {
                    foreach (var seanse in seanses)
                    {
                        var newRow = new VisitsAndSeansesRow();
                        newRow.Уход = "без посещения";
                        newRow.Тренер = seanse.Executor;
                        newRow.Подразделение = seanse.SectionName;
                        newRow.Занятие = seanse.Executed;
                        newRow.Блок = seanse.ServiceName;
                        newRow.Использовано = seanse.NumDone ?? "";
                        newRow.ВсегоВблоке = seanse.NumTotal ?? "";
                        tempList.Add(newRow);
                    }
                }

                if (Int32.Parse(visitAndSeansesRow.Занятия) > 0)
                {
                    totalRow.Посещения = (Int32.Parse(totalRow.Посещения) + Int32.Parse(visitAndSeansesRow.Посещения)).ToString();
                    totalRow.Занятия = (Int32.Parse(totalRow.Занятия) + Int32.Parse(visitAndSeansesRow.Занятия)).ToString();

                    list.Add(visitAndSeansesRow);
                    if (type != VisitsAndSeansesReportType.NOFILTER)
                        list.AddRange(tempList);
                }
            }

            list.Add(totalRow);

            return list;
        }

        public static List<List<string>> ToListList(List<VisitsAndSeansesRow> visitsAndSeanses)
        {
            var list = new List<List<string>>();
            var headerRow = new List<string>();
            headerRow.Add("НомерАнкеты");
            headerRow.Add("ФИО");
            headerRow.Add("Шаблон");
            headerRow.Add("ОстатокЛС");
            headerRow.Add("Кол посещений");
            headerRow.Add("Кол занятий");
            headerRow.Add("Приход");
            headerRow.Add("Уход");
            headerRow.Add("Занятие");
            headerRow.Add("Тренер");
            headerRow.Add("Подразделение");
            headerRow.Add("Блок");
            headerRow.Add("Использовано");
            headerRow.Add("Всего в блоке");
            list.Add(headerRow);

            foreach (var row in visitsAndSeanses)
            {
                var visitAndSeanse = new List<string>();

                visitAndSeanse.Add(row.НомерАнкеты);
                visitAndSeanse.Add(row.ФИО);
                visitAndSeanse.Add(row.Шаблон);
                visitAndSeanse.Add(row.ОстатокЛС);
                visitAndSeanse.Add(row.Посещения);
                visitAndSeanse.Add(row.Занятия);
                visitAndSeanse.Add(row.Приход);
                visitAndSeanse.Add(row.Уход);
                visitAndSeanse.Add(row.Занятие);
                visitAndSeanse.Add(row.Тренер);
                visitAndSeanse.Add(row.Подразделение);
                visitAndSeanse.Add(row.Блок);
                visitAndSeanse.Add(row.Использовано);
                visitAndSeanse.Add(row.ВсегоВблоке);

                list.Add(visitAndSeanse);
            }

            return list;
        }
    }
}
