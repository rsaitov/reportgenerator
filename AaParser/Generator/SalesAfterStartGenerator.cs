﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AaParser.Entity;

namespace AaParser.Generator
{
    public static class SalesAfterStartGenerator
    {
        
        //180101 Первичная стартовая тренировка
        //10713 Стартовая тренировка на выбор
        //30801 Стартовая тренировка Аэробика
        //20701 Стартовая тренировка бассейн
        //10404 Стартовая тренировка ТЗ
        //180201 Повторная стартовая тренировка
        //110405 Стартовое тестирование
        //51315	Стартовая сквош

        public static List<SalesAfterStartRow> Generate(string[] startCodes, List<SalesFromAaRow> salesFromAa, List<PhonesFromProfileRow> phonesFromProfile, List<RealizationFromAaRow> realizationFromAa, DateTime userStartDate)
        {
            //var startCodes = new string[] { "180101", 
            //    "10713", "010713", 
            //    "30801", "030801", 
            //    "20701", "020701",
            //    "10404", "010404",
            //    "180201", "110405", 
            //    "51315", "051315"};
            //var startCodes = new string[] { "51315" };

            var list = new List<SalesAfterStartRow>();
            var realizationFromAaFiltered = realizationFromAa.Where(x => startCodes.Contains(x.ServiceCode) && x.DatDt >= userStartDate).OrderBy(x => x.DatDt).ToList();
            var salesFromAaFiltered = salesFromAa.Where(x => x.PDataDt >= userStartDate).OrderBy(x => x.PDataDt).ToList();

            foreach (var row in realizationFromAaFiltered)
            {
                var personNumber = row.PersonNumber.Trim();

                var startDate = row.DatDt.Date;
                var salesRow = salesFromAaFiltered.Where(x => string.Equals(x.PersonNumber.Trim(), personNumber) && x.PDataDt.Date >= startDate);
                var phones = string.Join(" / ", phonesFromProfile.Where(x => string.Equals(x.PersonNumber.Trim(), personNumber)).Select(x => x.Phone));
                var exist = false;

                foreach (var sale in salesRow)
                {
                    var salesAfterStart = new SalesAfterStartRow();

                    salesAfterStart.Фио = row.Fio;
                    //salesAfterStart.Шаблон = "";
                    salesAfterStart.Стартовая = row.Name;
                    salesAfterStart.ДатаПрохождения = row.Dat;
                    salesAfterStart.ИсполнительСтарт = row.LevelName2;

                    salesAfterStart.ДатаПокупкиБлока = sale.Pdata;
                    salesAfterStart.НазваниеБлока = sale.SrvName;
                    salesAfterStart.ИсполнительПродажа = sale.Rname;
                    salesAfterStart.Телефоны = phones;

                    list.Add(salesAfterStart);

                    exist = true;
                }

                if (!exist)
                {
                    var salesAfterStart = new SalesAfterStartRow();

                    salesAfterStart.Фио = row.Fio;
                    //salesAfterStart.Шаблон = "";
                    salesAfterStart.Стартовая = row.Name;
                    salesAfterStart.ДатаПрохождения = row.Dat;
                    salesAfterStart.ИсполнительСтарт = row.LevelName2;

                    salesAfterStart.ДатаПокупкиБлока = "";
                    salesAfterStart.НазваниеБлока = "";
                    salesAfterStart.ИсполнительПродажа = "";
                    salesAfterStart.Телефоны = phones;

                    list.Add(salesAfterStart);
                }
            }

            return list;
        }

        public static List<List<string>> ToListList(List<SalesAfterStartRow> salesAfterStarts)
        {
            var list = new List<List<string>>();
            var headerRow = new List<string>();
            headerRow.Add("Фио");
            //headerRow.Add("Шаблон");
            headerRow.Add("Стартовая");
            headerRow.Add("Дата стартовой");
            headerRow.Add("Исполнитель");
            headerRow.Add("Дата продажи");
            headerRow.Add("Блок");
            headerRow.Add("Исполнитель");
            headerRow.Add("Телефоны");
            list.Add(headerRow);

            foreach (var row in salesAfterStarts)
            {
                var salesAfterStart = new List<string>();

                salesAfterStart.Add(row.Фио);
                //blocksLeftRow.Add(row.Шаблон);
                salesAfterStart.Add(row.Стартовая);
                salesAfterStart.Add(row.ДатаПрохождения);
                salesAfterStart.Add(row.ИсполнительСтарт);
                salesAfterStart.Add(row.ДатаПокупкиБлока);
                salesAfterStart.Add(row.НазваниеБлока);
                salesAfterStart.Add(row.ИсполнительПродажа);
                salesAfterStart.Add(row.Телефоны);

                list.Add(salesAfterStart);
            }

            return list;
        }
    }
}
