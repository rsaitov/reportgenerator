﻿using AaParser.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Generator
{
    public static class RetentionGenerator
    {
        //public static List<BlocksLeftRow> Generate(List<PhonesFromProfileRow> phonesFromProfile, List<RealizationFromAaRow> realization)
        //{

        //}


        public static bool IsPersonalTrain(string name, string code, string[] codes)
        {
            if (name.ToUpperInvariant().Contains(@"ПТ") ||
                name.ToUpperInvariant().Contains(@"СПЛИТ") ||
                name.ToUpperInvariant().Contains(@"ТРИО") ||
                name.ToUpperInvariant().Contains(@"КВАРТЕТ") ||
                codes.Any(x => code.StartsWith(x))
                )
            return true;

            return false;
        }

        public static bool IsGroupTrain(string name, string code, string[] codes)
        {
            if (name.ToUpperInvariant().Contains(@"СТУДИЯ") ||
                name.ToUpperInvariant().Contains(@"ГРУППА") ||
                name.ToUpperInvariant().Contains(@"СЕКЦИЯ") ||
                name.ToUpperInvariant().Contains(@"КОМ/ГР") ||
                codes.Any(x => code.StartsWith(x))
                )
                return true;

            return false;
        }
    }
}
