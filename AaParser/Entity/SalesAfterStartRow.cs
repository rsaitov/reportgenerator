﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class SalesAfterStartRow
    {
        public string Фио { get; set; }
        public string Шаблон { get; set; }
        public string Стартовая { get; set; }
        public string ДатаПрохождения { get; set; }
        public string ИсполнительСтарт { get; set; }
        public string ДатаПокупкиБлока { get; set; }
        public string НазваниеБлока { get; set; }
        public string ИсполнительПродажа { get; set; }
        public string Телефоны { get; set; }
    }
}
