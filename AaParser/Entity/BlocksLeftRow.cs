﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class BlocksLeftRow
    {
        public string Фио { get; set; }
        public string Шаблон { get; set; }
        public string Услуга { get; set; }
        public string Остаток { get; set; }
        public string Исполнитель { get; set; }
        public string Создан { get; set; }
        public string Чек { get; set; }
        public string ТипОплаты { get; set; }
        public string ОстатокВколичестве { get; set; }
        public string Телефоны { get; set; }
    }
}
