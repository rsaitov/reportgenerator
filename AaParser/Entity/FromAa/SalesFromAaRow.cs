﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class SalesFromAaRow
    {
        public string Docn { get; set; }
        public string SrvName { get; set; }
        public string Quantity { get; set; }
        public string SData { get; set; }
        public string Price { get; set; }
        public string Discount { get; set; }
        public string Rname { get; set; }
        public string Operator { get; set; }
        public string GrpName { get; set; }
        public string Fio { get; set; }
        public string Pdata { get; set; }
        public string Summa { get; set; }
        public string RecordId { get; set; }
        public string Pcode { get; set; }
        public string PdataG { get; set; }
        public string PayType { get; set; }
        public string Comment { get; set; }
        public string TotalPrice { get; set; }

        public string SrvCode { get; set; }
        public string PersonNumber { get; set; }
        public DateTime PDataDt { get; set; }
    }
}
