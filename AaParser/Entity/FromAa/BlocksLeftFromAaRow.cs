﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class BlocksLeftFromAaRow
    {
        public string GroupName { get; set; }
        public string CType { get; set; }
        public string Status { get; set; }
        public string Fio { get; set; }
        public string Card { get; set; }
        public string StData { get; set; }
        public string SpData { get; set; }
        public string Info { get; set; }
        public string Id { get; set; }
        public string ServiceId { get; set; }
        public string Sname { get; set; }
        public string Itog { get; set; }
        public string NPeriod { get; set; }
        public string Created { get; set; }
        public string CreatedBy { get; set; }
        public string GroupId { get; set; }
        public string ExpiredDate { get; set; }
        public string TotalQuantity { get; set; }
        public string TotalDone { get; set; }
        public string Resource { get; set; }
        public string PersonNumber { get; set; }

        public DateTime CreatedDt { get; set; }
    }
}
