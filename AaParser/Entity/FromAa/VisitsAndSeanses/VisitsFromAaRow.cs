﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class VisitsFromAaRow
    {
        public string НомерАнкеты { get; set; }
        public string EnterTime { get; set; }
        public string ExitTime { get; set; }

        public DateTime EnterTimeDateTime { get; set; }
        public DateTime ExitTimeDateTime { get; set; }
    }
}
