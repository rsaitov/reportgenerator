﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class ActiveContractsFromAaRow
    {
        public string НомерАнкеты { get; set; }
        public string Фамилия { get; set; }
        public string Имя { get; set; }
        public string Отчество { get; set; }
        public string TemplateName { get; set; }
        public string DateFrom { get; set; }
        public string DateToWFreeze { get; set; }
        public string Card { get; set; }
        public string Status { get; set; }
        public string AccountNumber { get; set; }
    }
}
