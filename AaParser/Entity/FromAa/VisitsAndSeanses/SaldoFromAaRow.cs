﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class SaldoFromAaRow
    {
        public string AccountNumber { get; set; }
        public string Saldo { get; set; }
    }
}
