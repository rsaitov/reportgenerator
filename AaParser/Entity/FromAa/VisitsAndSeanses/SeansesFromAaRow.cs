﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class SeansesFromAaRow
    {
        public string НомерАнкеты { get; set; }
        public string SectionName { get; set; }
        public string NumDone { get; set; }
        public string NumTotal { get; set; }
        public string Executed { get; set; }
        public string Executor { get; set; }
        public string ServiceName { get; set; }

        public DateTime ExecutedDateTime { get; set; }
    }
}
