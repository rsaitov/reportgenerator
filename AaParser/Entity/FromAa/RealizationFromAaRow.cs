﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class RealizationFromAaRow
    {
        public string GrpLevel1 { get; set; }
        public string LevelName1 { get; set; }
        public string GrpLevel2 { get; set; }
        public string LevelName2 { get; set; }
        public string Fio { get; set; }
        public string Dat { get; set; }
        public string Price { get; set; }
        public string Quant { get; set; }
        public string Summa { get; set; }
        public string Name { get; set; }
        public string DetailClient { get; set; }
        public string QuantFree { get; set; }
        public string TimeMes { get; set; }
        public string DetailDate { get; set; }
        public string PayType { get; set; }
        public string ServiceCode { get; set; }
        public string ResourceId { get; set; }

        public string PersonNumber { get; set; }
        public DateTime DatDt { get; set; }
    }
}
