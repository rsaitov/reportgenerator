﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class PhonesFromProfileRow
    {
        public string PersonNumber { get; set; }
        public string Phone { get; set; }
    }
}
