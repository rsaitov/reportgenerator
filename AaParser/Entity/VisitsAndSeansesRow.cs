﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class VisitsAndSeansesRow : ICloneable
    {
        public string НомерАнкеты { get; set; }
        public string ФИО { get; set; }
        public string Шаблон { get; set; }
        public string ОстатокЛС { get; set; }
        public string Посещения { get; set; }
        public string Занятия { get; set; }
        public string Приход { get; set; }
        public string Уход { get; set; }
        public string Тренер { get; set; }
        public string Подразделение { get; set; }
        public string Занятие { get; set; }
        public string Блок { get; set; }
        public string Использовано { get; set; }
        public string ВсегоВблоке { get; set; }

        public VisitsAndSeansesRow()
        {
            НомерАнкеты = "";
            ФИО = "";
            Шаблон = "";
            Посещения = "";
            Занятия = "";
            Приход = "";
            Уход = "";
            Тренер = "";
            Подразделение = "";
            Занятие = "";
            ОстатокЛС = "";
            Блок = "";
            Использовано = "";
            ВсегоВблоке = "";
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
