﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AaParser.Entity
{
    public class RetentionRow
    {
        public string Department { get; set; }
        public string Executor { get; set; }
    }
}
