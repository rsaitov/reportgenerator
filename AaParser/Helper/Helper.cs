﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AaParser.Entity;

namespace AaParser.Helper
{
    public static class ExcelGenerator
    {
        public static bool Generate(List<List<string>> data, string fileName)
        {
            var dataInXml2003Format = new StringBuilder();
            dataInXml2003Format.Append(AppendHeader());

            dataInXml2003Format.Append(" <Worksheet ss:Name=\"Лист 1\">\r\n" +
                                       " <Table>");
            foreach (var column in data[0])
                dataInXml2003Format.Append(" <Column ss:AutoFitWidth=\"0\" ss:Width=\"80\"/>");
            dataInXml2003Format.Append("\r\n");

            var excelStyle1 = new ExcelStyle();
            excelStyle1.Id = 1;
            excelStyle1.IsNumber = false;

            var excelStyle2 = new ExcelStyle();
            excelStyle2.Id = 2;
            excelStyle2.IsNumber = false;

            var list = new List<ExcelRow>();
            bool header = true;
            foreach (var row in data)
            {
                var excelRow = new ExcelRow();
                foreach (var column in row)
                {
                    var cell = new ExcelCell {sValue = column};
                    cell.Style = header ? excelStyle2 : excelStyle1;
                    excelRow.Cells.Add(cell);
                    
                }
                list.Add(excelRow);
                header = false;
            }

            foreach (var row in list)
            {
                dataInXml2003Format.Append("<Row>\r\n");
                for (byte i = 0; i < row.Cells.Count; i++)
                {
                    var cell = row.Cells[i];
                    if (cell.nMergeHorizontal == 0)
                        dataInXml2003Format.Append("<Cell ss:StyleID=\"s" + cell.Style.Id + "\"><Data ss:Type=\"" + (cell.Style.IsNumber ? @"Number" : @"String") +
                                                   "\">" + cell.sValue.Replace(',', '.') + "</Data></Cell>\r\n");
                    else
                    {
                        dataInXml2003Format.Append("<Cell ss:MergeAcross=\"" + cell.nMergeHorizontal + "\" ss:StyleID=\"s" +
                                                   cell.Style.Id + "\"><Data ss:Type=\"" + (cell.Style.IsNumber ? @"Number" : @"String") + "\">" +
                                                   cell.sValue.Replace(',', '.') + "</Data></Cell>\r\n");
                        i += cell.nMergeHorizontal;
                    }
                }

                dataInXml2003Format.Append("</Row>\r\n");
            }

            dataInXml2003Format.Append(
                "  </Table>\r\n" +
                "  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\r\n" +
                "<Selected/>\r\n" +
                "<FreezePanes/>\r\n" +
                "<FrozenNoSplit/>\r\n" +
                "<LeftColumnRightPane>1</LeftColumnRightPane>\r\n" +
                "<ActivePane>1</ActivePane>\r\n" +
                "<Panes>\r\n" +
                " <Pane>\r\n" +
                "  <Number>3</Number>\r\n" +
                " </Pane>\r\n" +
                " <Pane>\r\n" +
                "  <Number>1</Number>\r\n" +
                "  <ActiveCol>0</ActiveCol>\r\n" +
                " </Pane>\r\n" +
                "</Panes>\r\n" +
                "   <ProtectObjects>False</ProtectObjects>\r\n" +
                "   <ProtectScenarios>False</ProtectScenarios>\r\n" +
                "  </WorksheetOptions>\r\n" +
                " </Worksheet>\r\n");

            dataInXml2003Format.Append("</Workbook>");

            ReportSaver.SaveDataToFile(fileName, dataInXml2003Format);
            ReportSaver.SaveInXlsxFormat(fileName);
            ReportSaver.DeleteFile(ReportSaver.GetNoXsDialogFileName(fileName));

            return true;
        }

        private static string AppendHeader()
        {
            return " <?xml version=\"1.0\"?> \r\n" +
                   " <?mso-application progid=\"Excel.Sheet\"?> \r\n" +
                   " <Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" +
                   " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n" +
                   " xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\r\n" +
                   " xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" +
                   " xmlns:html=\"http://www.w3.org/TR/REC-html40\">\r\n" +
                   " <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">\r\n" +
                   " <Author>pro3</Author>\r\n" +
                   "  <LastAuthor>Rinat Saitov</LastAuthor>\r\n" +
                   "  <Created>" + DateTime.Now + "</Created>\r\n" +
                   "  <Company>Fitness Holding</Company>\r\n" +
                   "  <Version>12.00</Version>\r\n" +
                   " </DocumentProperties>\r\n" +
                   " <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">\r\n" +
                   "  <WindowHeight>10620</WindowHeight>\r\n" +
                   "  <WindowWidth>19020</WindowWidth>\r\n" +
                   "  <WindowTopX>480</WindowTopX>\r\n" +
                   "  <WindowTopY>15</WindowTopY>\r\n" +
                   "  <ProtectStructure>False</ProtectStructure>\r\n" +
                   "  <ProtectWindows>False</ProtectWindows>\r\n" +
                   " </ExcelWorkbook>\r\n" +
                   "<Styles>\r\n" +

                   // обычный с границами
                   "	<Style ss:ID=\"s1\">\r\n" +
                   "		<Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Center\"/>\r\n" +
                   "		<Borders>\r\n" +
                   "			<Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>\r\n" +
                   "			<Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>\r\n" +
                   "			<Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>\r\n" +
                   "			<Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>\r\n" +
                   "		</Borders>\r\n" +
                   "		<Font ss:FontName=\"Calibri\" x:CharSet=\"204\" x:Family=\"Swiss\" ss:Size=\"9\" ss:Color=\"#000000\"/>\r\n" +
                   "	</Style>\r\n" +

                   // жирный с границами
                   "	<Style ss:ID=\"s2\">\r\n" +
                   "		<Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Center\"/>\r\n" +
                   "		<Borders>\r\n" +
                   "			<Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>\r\n" +
                   "			<Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>\r\n" +
                   "			<Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>\r\n" +
                   "			<Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>\r\n" +
                   "		</Borders>\r\n" +
                   "		<Font ss:FontName=\"Calibri\" x:CharSet=\"204\" x:Family=\"Swiss\" ss:Size=\"9\" ss:Color=\"#000000\" ss:Bold=\"1\"/>\r\n" +
                   "	</Style>\r\n" +

                   "</Styles>\r\n";
        }
    }

    public class ExcelRow
    {
        public List<ExcelCell> Cells;

        public ExcelRow()
        {
            Cells = new List<ExcelCell>();
        }
    }
    public class ExcelCell
    {
        public string sValue;
        public string sStyle;
        public string sData;

        public byte nMergeHorizontal;
        public byte nMergeVertical;

        public ExcelStyle Style { get; set; }

        public ExcelCell()
        {
            sValue = "";
            sStyle = "s1";
            sData = "String";

            Style = new ExcelStyle();
        }
    }
    public enum ExcelAlignment { Left, Center, Right, Top, Bottom }
    public enum ExcelBorders { No, All, Top, Bottom, Left, Right }
    public class ExcelStyle
    {
        public int Id { get; set; }
        public ExcelAlignment AlignmentHorizontal { get; set; }
        public ExcelAlignment AlignmentVertical { get; set; }
        public bool WrapText { get; set; }
        public ExcelBorders Borders { get; set; }
        public int FontSize { get; set; }
        public string FontColor { get; set; }
        public bool FontBold { get; set; }
        public bool FontItalic { get; set; }
        public string BackgroundColor { get; set; }
        public bool IsNumber { get; set; }
        public string NumberFormat { get; set; }

        public ExcelStyle()
        {
            AlignmentHorizontal = ExcelAlignment.Left;
            AlignmentVertical = ExcelAlignment.Center;
            WrapText = false;
            Borders = ExcelBorders.No;
            FontSize = 8;
            FontColor = @"000000";
            FontBold = false;
            FontItalic = false;
            BackgroundColor = @"FFFFFF";
            NumberFormat = @"@";
        }

        public ExcelStyle Clone()
        {
            var newExcelStyle = new ExcelStyle();
            newExcelStyle.AlignmentHorizontal = AlignmentHorizontal;
            newExcelStyle.AlignmentVertical = AlignmentVertical;
            newExcelStyle.WrapText = WrapText;
            newExcelStyle.Borders = Borders;
            newExcelStyle.FontSize = FontSize;
            newExcelStyle.FontColor = FontColor;
            newExcelStyle.FontBold = FontBold;
            newExcelStyle.FontItalic = FontItalic;
            newExcelStyle.BackgroundColor = BackgroundColor;
            newExcelStyle.IsNumber = IsNumber;
            newExcelStyle.NumberFormat = NumberFormat;
            return newExcelStyle;
        }

        public bool Equals(ExcelStyle style)
        {
            return AlignmentHorizontal == style.AlignmentHorizontal &&
                   AlignmentVertical == style.AlignmentVertical &&
                   WrapText == style.WrapText &&
                   Borders == style.Borders &&
                   FontSize == style.FontSize &&
                   FontColor == style.FontColor &&
                   FontBold == style.FontBold &&
                   FontItalic == style.FontItalic &&
                   BackgroundColor == style.BackgroundColor &&
                   IsNumber == style.IsNumber &&
                   NumberFormat == style.NumberFormat;
        }

        public string Serialize(List<ExcelStyle> styles)
        {
            var stringStyle = new StringBuilder();

            var borders = @"";

            if (Borders != ExcelBorders.No)
            {
                var top = Borders == ExcelBorders.All || Borders == ExcelBorders.Top ? @"1" : @"0";
                var bottom = Borders == ExcelBorders.All || Borders == ExcelBorders.Bottom ? @"1" : @"0";
                var left = Borders == ExcelBorders.All || Borders == ExcelBorders.Left ? @"1" : @"0";
                var right = Borders == ExcelBorders.All || Borders == ExcelBorders.Right ? @"1" : @"0";

                borders = string.Format(
                    "		<Borders>\r\n" +
                    "			<Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"{0}\"/>\r\n" +
                    "			<Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"{1}\"/>\r\n" +
                    "			<Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"{2}\"/>\r\n" +
                    "			<Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"{3}\"/>\r\n" +
                    "		</Borders>\r\n",
                    top, bottom, left, right
                );
            }

            var interior = BackgroundColor == @"FFFFFF" ? @"" : string.Format("		<Interior ss:Color=\"#{0}\" ss:Pattern=\"Solid\"/>\r\n", BackgroundColor);

            stringStyle.Append(
                string.Format(
                    "   <Style ss:ID=\"s{9}\">\r\n" +
                    "		<Alignment ss:Horizontal=\"{0}\" ss:Vertical=\"{1}\" ss:WrapText=\"{2}\"/>\r\n" +
                    "{3}" +
                    "		<Font ss:FontName=\"Arial\" x:CharSet=\"204\" x:Family=\"Swiss\" ss:Size=\"{4}\" ss:Color=\"#{5}\" ss:Bold=\"{6}\" ss:Italic=\"{10}\"/>\r\n" +
                    "{7}" +
                    "		<NumberFormat ss:Format=\"{8}\"/>\r\n" +
                    "	</Style>",
                    AlignmentHorizontal,
                    AlignmentVertical,
                    WrapText ? "1" : "0",
                    borders,
                    FontSize,
                    FontColor,
                    FontBold ? "1" : "0",
                    interior,
                    NumberFormat,
                    Id,
                    FontItalic ? "1" : "0")
                );


            return stringStyle.ToString();
        }

        public void MakeId(List<ExcelStyle> styles)
        {
            if (Id < 1)
            {
                Id = 1;
                foreach (var style in styles)
                    if (style.Id >= Id)
                        Id = style.Id + 1;
            }
        }
    }


}
