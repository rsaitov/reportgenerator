﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelM = Microsoft.Office.Interop.Excel;
using System.IO;

namespace AaParser.Helper
{
    public static class ReportSaver
    {
        public static void SaveDataToFile(string sDialogFileName, StringBuilder sToExcel)
        {
            string noXsDialogFileName = ReportSaver.GetNoXsDialogFileName(sDialogFileName);
            System.IO.File.WriteAllText(noXsDialogFileName, sToExcel.ToString());
        }
        public static string SaveInXlsxFormat(string sDialogFileName)
        {
            ExcelM.Application xlApp = new ExcelM.Application();
            string noXsDialogFileName = ReportSaver.GetNoXsDialogFileName(sDialogFileName);
            ExcelM._Workbook xlWorkBook = xlApp.Workbooks.Open(noXsDialogFileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlApp.DisplayAlerts = false;
            xlWorkBook.SaveAs(sDialogFileName, ExcelM.XlFileFormat.xlOpenXMLWorkbook, Type.Missing, Type.Missing, Type.Missing, Type.Missing, ExcelM.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            xlApp.DisplayAlerts = true;
            xlWorkBook.Close(false, Type.Missing, Type.Missing);
            xlApp.Quit();

            return noXsDialogFileName;
        }
        public static string GetNoXsDialogFileName(string fileName)
        {
            return fileName.Remove(fileName.Length - 1); ;
        }
        public static void DeleteFile(string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
        }
        public static void OpenFileInExcel(string fileName)
        {
            using (var proc = new System.Diagnostics.Process())
            {
                proc.StartInfo.FileName = fileName;
                proc.StartInfo.UseShellExecute = true;
                proc.Start();
            }
        }
    }
}
