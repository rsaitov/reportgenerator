﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AaParser.Generator;
using AaParser.Helper;
using AaParser.Parser;

namespace RepGen.Controllers
{
    public class SalesAfterStartController : Controller
    {
        //
        // GET: /SalesAfterStart/
        public ActionResult Index()
        {
            var storageFilesFolder = ConfigurationManager.AppSettings["StorageFilesFolder"];
            var directoryInfo = new DirectoryInfo(storageFilesFolder);
            var fsi = directoryInfo.GetFileSystemInfos();

            foreach (var info in fsi)
            {
                if (info.Name.StartsWith("02"))
                    ViewBag.Report02 = info;

                if (info.Name.StartsWith("03"))
                    ViewBag.Report03 = info;

                if (info.Name.StartsWith("04"))
                    ViewBag.Report04 = info;
            }

            ViewBag.DateStart = DateTime.Now.Date.AddMonths(-2);
            ViewBag.StartCodes = ConfigurationManager.AppSettings["StartCodes"];

            return View();
        }

        [HttpPost]
        public FileResult Auto(DateTime dateStart)
        {
            var storageFilesFolder = ConfigurationManager.AppSettings["StorageFilesFolder"];
            var directoryInfo = new DirectoryInfo(storageFilesFolder);
            var fsi = directoryInfo.GetFileSystemInfos();

            FileSystemInfo report02 = null, report03 = null, report04 = null;

            foreach (var info in fsi)
            {
                if (info.Name.StartsWith("02"))
                    report02 = info;

                if (info.Name.StartsWith("03"))
                    report03 = info;

                if (info.Name.StartsWith("04"))
                    report04 = info;
            }

            return Generate(report02.FullName, report03.FullName, report04.FullName, dateStart);
        }

        public ActionResult Manual()
        {
            return View();
        }

        [HttpPost]
        public FileResult SalesAfterStart(SalesAfterStartModel model)
        {
            var uploadFilesFolder = ConfigurationManager.AppSettings["UploadFilesFolder"];
            
            var fileNameReportChecksFromAa = Path.GetFileName(model.ReportChecksFromAa.FileName);
            model.ReportChecksFromAa.SaveAs(uploadFilesFolder + fileNameReportChecksFromAa);

            var fileNameReportReportPhonesFromProfile = Path.GetFileName(model.ReportPhonesFromProfile.FileName);
            model.ReportPhonesFromProfile.SaveAs(uploadFilesFolder + fileNameReportReportPhonesFromProfile);

            var fileNameReportRealizationFromAa = Path.GetFileName(model.ReportRealizationFromAa.FileName);
            model.ReportRealizationFromAa.SaveAs(uploadFilesFolder + fileNameReportRealizationFromAa);
            
            var report02 = uploadFilesFolder + fileNameReportChecksFromAa;
            var report03 = uploadFilesFolder + fileNameReportReportPhonesFromProfile;
            var report04 = uploadFilesFolder + fileNameReportRealizationFromAa;

            return Generate(report02, report03, report04, model.DateStart);
        }
        private FileResult Generate(string report02, string report03, string report04, DateTime dateStart)
        {
            var resultFilesFolder = ConfigurationManager.AppSettings["ResultFilesFolder"];

            var salesFromAa = ReportFromAaParser.ParseSalesFromAa(report02);
            var phonesFromProfile = ReportFromAaParser.ParsePhonesFromProfile(report03);
            var realizationFromAa = ReportFromAaParser.ParseRealizationFromAa(report04);

            var startCodesFromWebConfig = ConfigurationManager.AppSettings["StartCodes"].Split(' ');
            var salesAfterStart = SalesAfterStartGenerator.Generate(startCodesFromWebConfig, salesFromAa, phonesFromProfile, realizationFromAa, dateStart);
            var salesAfterStartLeftList = SalesAfterStartGenerator.ToListList(salesAfterStart);

            var fileName = DateTime.Now.ToString("s").Replace(":", "_") + @".xlsx";
            var fileResult = resultFilesFolder + "ПродажаПослеСтартовой_" + fileName;
            ExcelGenerator.Generate(salesAfterStartLeftList, fileResult);

            byte[] fileBytes = System.IO.File.ReadAllBytes(fileResult);
            var responce = new FileContentResult(fileBytes, "application/octet-stream");
            responce.FileDownloadName = fileName;

            return responce;
        }
	}

    public class SalesAfterStartModel
    {
        public DateTime DateStart { get; set; }
        public HttpPostedFileBase ReportChecksFromAa { get; set; }
        public HttpPostedFileBase ReportPhonesFromProfile { get; set; }
        public HttpPostedFileBase ReportRealizationFromAa { get; set; }
    }
}