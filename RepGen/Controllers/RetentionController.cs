﻿using AaParser.Entity;
using AaParser.Generator;
using AaParser.Parser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RepGen.Controllers
{
    public class RetentionController : Controller
    {
        //
        // GET: /Retention/
        public ActionResult Index()
        {
            var storageFilesFolder = ConfigurationManager.AppSettings["StorageFilesFolder"];
            var directoryInfo = new DirectoryInfo(storageFilesFolder);
            var fsi = directoryInfo.GetFileSystemInfos();

            foreach (var info in fsi)
            {
                if (info.Name.StartsWith("03"))
                    ViewBag.Report03 = info;

                if (info.Name.StartsWith("04"))
                    ViewBag.Report04 = info;
            }

            FileSystemInfo report03 = null, report04 = null;

            foreach (var info in fsi)
            {
                if (info.Name.StartsWith("03"))
                    report03 = info;

                if (info.Name.StartsWith("04"))
                    report04 = info;
            }

            var report03s = storageFilesFolder + Path.GetFileName(report03.FullName);
            var report04s = storageFilesFolder + Path.GetFileName(report04.FullName);
            var phonesFromProfile = ReportFromAaParser.ParsePhonesFromProfile(report03s);
            var realizationFromAa = ReportFromAaParser.ParseRealizationFromAa(report04s);

            var model = new RetentionViewModel();
            model.Departments = realizationFromAa.OrderBy(x => x.LevelName1).Select(x => x.LevelName1).Distinct().ToList();
            model.Years = realizationFromAa.OrderByDescending(x => x.DatDt).Select(x => x.DatDt.Year).Distinct().ToList();
            model.Executors = realizationFromAa.OrderBy(x => x.GrpLevel2).Select(x => x.GrpLevel2).Distinct().ToList();

            Session["realization"] = realizationFromAa;
            Session["Executors"] = model.Executors;
            Session["Years"] = model.Years;
            Session["Departments"] = model.Departments;

            return View(model);
        }

        [HttpPost]
        public ActionResult Auto(YearViewModel model)
        {
            var realization = Session["realization"] as List<RealizationFromAaRow>;
            var years = Session["Years"] as List<int>;
            var departments = Session["Departments"] as List<string>;

            if (model.Year == 0)
                model.Year = years.Max();

            if (model.Service == 0)
                model.Service = 1;

            if (model.Show == 0)
                model.Show = 1;

            if (model.Department == null)
                model.Department = departments.First(x => x.Contains("нажерн"));

            var realizationYearAndDepartment = realization.Where(x => string.Equals(x.LevelName1, model.Department) && x.DatDt.Year == model.Year && x.Summa != @"0").ToList();
            var realizationPrevYearAndDepartment = realization.Where(x => string.Equals(x.LevelName1, model.Department) && x.DatDt.Year == model.Year - 1 && x.Summa != @"0").ToList();

            var ptCodes = ConfigurationManager.AppSettings["PTCodes"].Split(' ');
            var studioCodes = ConfigurationManager.AppSettings["StudioCodes"].Split(' ');

            //ПТ Сплит Трио Квартет
            if (model.Service == 2)
            {
                realizationYearAndDepartment = realizationYearAndDepartment.Where(x => RetentionGenerator.IsPersonalTrain(x.Name, x.ServiceCode, ptCodes)).ToList();
                realizationPrevYearAndDepartment = realizationPrevYearAndDepartment.Where(x => RetentionGenerator.IsPersonalTrain(x.Name, x.ServiceCode, ptCodes)).ToList();
            }
            else if (model.Service == 3)
            {
                realizationYearAndDepartment = realizationYearAndDepartment.Where(x => RetentionGenerator.IsGroupTrain(x.Name, x.ServiceCode, studioCodes)).ToList();
                realizationPrevYearAndDepartment = realizationPrevYearAndDepartment.Where(x => RetentionGenerator.IsGroupTrain(x.Name, x.ServiceCode, studioCodes)).ToList();
            }

            DepartmentViewModel departmentViewModel = new DepartmentViewModel();
            departmentViewModel.Show = model.Show;
            departmentViewModel.Service = model.Service;
            departmentViewModel.Years = years;
            departmentViewModel.Year = model.Year;
            departmentViewModel.Departments = departments;
            departmentViewModel.Department = model.Department;
            departmentViewModel.Executors = new List<ExecutorYearInfo>();

            var rand = new Random();
            var executors = realizationYearAndDepartment.OrderBy(x => x.GrpLevel2).Select(x => x.GrpLevel2).Distinct().ToList();
            foreach (var item in executors)
            {
                var executor = new ExecutorYearInfo();
                executor.Retention = new List<List<string>>();
                executor.Total = new List<List<string>>();
                executor.RetentionPercent = new List<decimal>();
                executor.Name = item;

                var realizationYearDepartmentExecutor = realizationYearAndDepartment.Where(x => string.Equals(x.GrpLevel2, item)).ToList();
                var realizationPrevYearDepartmentExecutor = realizationPrevYearAndDepartment.Where(x => string.Equals(x.GrpLevel2, item)).ToList();

                for (int i = 1; i < 13; i++)
                {
                    var realizationPrevMonth = realizationYearDepartmentExecutor.Where(x => x.DatDt.Month == i - 1).ToList();
                    var realizationMonth = realizationYearDepartmentExecutor.Where(x => x.DatDt.Month == i).ToList();

                    var clientsPrev = realizationPrevMonth.Select(x => x.Fio).Distinct().ToList();
                    if (i == 1)
                    {
                        var realizationPrevYearDecember = realizationPrevYearDepartmentExecutor.Where(x => x.DatDt.Month == 12).ToList();
                        clientsPrev = realizationPrevYearDecember.Select(x => x.Fio).Distinct().ToList();
                    }
                    var clients = realizationMonth.Select(x => x.Fio).Distinct().ToList();
                    executor.Total.Add(clients);

                    var retentionClients = clients.Where(x => clientsPrev.Contains(x)).ToList();
                    executor.Retention.Add(retentionClients);

                    var value = clientsPrev.Count != 0 ? retentionClients.Count * 100 / clientsPrev.Count : 0;

                    executor.RetentionPercent.Add(value);
                }

                departmentViewModel.Executors.Add(executor);
            }

            return View(departmentViewModel);
        }

        [HttpPost]
        public ActionResult Executor(ExecutorViewModel model)
        {
            return View();
        }
	}

    public class RetentionViewModel
    {
        public List<int> Years { get; set; }
        public List<string> Departments { get; set; }
        public List<string> Executors { get; set; }
    }


    public class YearViewModel
    {        
        public int Year { get; set; }
        public string Department { get; set; }
        public int Show { get; set; }
        public int Service { get; set; }
    }

    public class ExecutorYearInfo
    {
        public string Name { get; set; }
        public List<List<string>> Total { get; set; }        
        public List<List<string>> Retention { get; set; }
        public List<decimal> RetentionPercent { get; set; }     
    }

    public class DepartmentViewModel
    {
        public List<int> Years { get; set; }
        public List<string> Departments { get; set; }
        public int Year { get; set; }
        public string Department { get; set; }
        public List<ExecutorYearInfo> Executors { get; set; }
        public int Show { get; set; }
        public int Service { get; set; }
    }

    public class ExecutorViewModel
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string Department { get; set; }
        public string Executors { get; set; }
    }
}