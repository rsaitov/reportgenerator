﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AaParser.Generator;
using AaParser.Helper;
using AaParser.Parser;
using AaParser.Linker;

namespace RepGen.Controllers
{    
    public class VisitsAndSeansesController : Controller
    {
        // GET: VisitsAndSeanses
        public ActionResult Index()
        {
            var storageFilesFolder = ConfigurationManager.AppSettings["StorageFilesFolder"];
            var directoryInfo = new DirectoryInfo(storageFilesFolder);
            var fsi = directoryInfo.GetFileSystemInfos();

            foreach (var info in fsi)
            {
                // активные контракты
                if (info.Name.StartsWith("05"))
                    ViewBag.Report05 = info;

                // посещения
                if (info.Name.StartsWith("06"))
                    ViewBag.Report06 = info;

                // сеансы
                if (info.Name.StartsWith("07"))
                    ViewBag.Report07 = info;

                // сальдо
                if (info.Name.StartsWith("08"))
                    ViewBag.Report08 = info;
            }

            return View();
        }

        public FileResult Auto(VisitsAndSeansesReportType type)
        {
            var storageFilesFolder = ConfigurationManager.AppSettings["StorageFilesFolder"];
            var directoryInfo = new DirectoryInfo(storageFilesFolder);
            var fsi = directoryInfo.GetFileSystemInfos();

            FileSystemInfo report05 = null, report06 = null, report07 = null, report08 = null;

            foreach (var info in fsi)
            {
                if (info.Name.StartsWith("05"))
                    report05 = info;

                if (info.Name.StartsWith("06"))
                    report06 = info;

                if (info.Name.StartsWith("07"))
                    report07 = info;

                if (info.Name.StartsWith("08"))
                    report08 = info;
            }

            return Generate(report05.FullName, report06.FullName, report07.FullName, report08.FullName, type);
        }

        private FileResult Generate(string report05, string report06, string report07, string report08, VisitsAndSeansesReportType type)
        {
            //ILinker linker = new lCLinker();
            ILinker linker = LinkerFactory.CreateLinker(report08);

            var resultFilesFolder = ConfigurationManager.AppSettings["ResultFilesFolder"];

            var activeContractsFromAa = ReportFromAaParser.ParseActiveContracts(report05);
            var visitsFromAa = ReportFromAaParser.ParseVisits(report06);
            var seansesFromAa = ReportFromAaParser.ParseSeanses(report07);
            //var saldoFromAa = ReportFromAaParser.ParseSaldo(report08);

            linker.LoadSaldo(report08);

            var visitsAndSeanses = VisitsAndSeansesGenerator.Generate(activeContractsFromAa, visitsFromAa, seansesFromAa, linker, type);
            var visitsAndSeansesList = VisitsAndSeansesGenerator.ToListList(visitsAndSeanses);

            var fileName = DateTime.Now.ToString("s").Replace(":", "_") + @".xlsx";
            var fileResult = $"{resultFilesFolder}Посещения_и_сеансы_{type.ToString()}_{fileName}";
            ExcelGenerator.Generate(visitsAndSeansesList, fileResult);

            byte[] fileBytes = System.IO.File.ReadAllBytes(fileResult);
            var responce = new FileContentResult(fileBytes, "application/octet-stream");
            responce.FileDownloadName = fileName;            

            return responce;
        }
    }
}