﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AaParser.Generator;
using AaParser.Helper;
using AaParser.Parser;

namespace RepGen.Controllers
{
    public class BlocksLeftController : Controller
    {
        //
        // GET: /BlocksLeft/
        public ActionResult Index()
        {
            var storageFilesFolder = ConfigurationManager.AppSettings["StorageFilesFolder"];
            var directoryInfo = new DirectoryInfo(storageFilesFolder);
            var fsi = directoryInfo.GetFileSystemInfos();

            foreach (var info in fsi)
            {
                if (info.Name.StartsWith("01"))
                    ViewBag.Report01 = info;

                if (info.Name.StartsWith("02"))
                    ViewBag.Report02 = info;

                if (info.Name.StartsWith("03"))
                    ViewBag.Report03 = info;
            }
            
            return View();
        }

        public FileResult Auto()
        {
            var storageFilesFolder = ConfigurationManager.AppSettings["StorageFilesFolder"];
            var directoryInfo = new DirectoryInfo(storageFilesFolder);
            var fsi = directoryInfo.GetFileSystemInfos();

            FileSystemInfo report01 = null , report02 = null, report03 = null;

            foreach (var info in fsi)
            {
                if (info.Name.StartsWith("01"))
                    report01 = info;

                if (info.Name.StartsWith("02"))
                    report02 = info;

                if (info.Name.StartsWith("03"))
                    report03 = info;
            }

            return Generate(report01.FullName, report02.FullName, report03.FullName);
        }

        public ActionResult Manual()
        {
            return View();
        }

        public ActionResult Report01()
        {
            return View();
        }

        public ActionResult Report02()
        {
            return View();
        }

        [HttpPost]
        public FileResult BlocksLeft(BlocksLeftModel model)
        {
            var uploadFilesFolder = ConfigurationManager.AppSettings["UploadFilesFolder"];

            var fileNameReportBlocksLeftFromAa = Path.GetFileName(model.ReportBlocksLeftFromAa.FileName);
            model.ReportBlocksLeftFromAa.SaveAs(uploadFilesFolder + fileNameReportBlocksLeftFromAa);

            var fileNameReportChecksFromAa = Path.GetFileName(model.ReportChecksFromAa.FileName);
            model.ReportChecksFromAa.SaveAs(uploadFilesFolder + fileNameReportChecksFromAa);

            var fileNameReportReportPhonesFromProfile = Path.GetFileName(model.ReportPhonesFromProfile.FileName);
            model.ReportPhonesFromProfile.SaveAs(uploadFilesFolder + fileNameReportReportPhonesFromProfile);

            var report01 = uploadFilesFolder + fileNameReportBlocksLeftFromAa;
            var report02 = uploadFilesFolder + fileNameReportChecksFromAa;
            var report03 = uploadFilesFolder + fileNameReportReportPhonesFromProfile;

            return Generate(report01, report02, report03);
        }

        private FileResult Generate(string report01, string report02, string report03)
        {
            var resultFilesFolder = ConfigurationManager.AppSettings["ResultFilesFolder"];

            var blocksLeftFromAa = ReportFromAaParser.ParseBlocksLeftFromAa(report01);
            var salesFromAa = ReportFromAaParser.ParseSalesFromAa(report02);
            var phonesFromProfile = ReportFromAaParser.ParsePhonesFromProfile(report03);

            var blocksLeft = BlocksLeftGenerator.Generate(blocksLeftFromAa, salesFromAa, phonesFromProfile);
            var blocksLeftList = BlocksLeftGenerator.ToListList(blocksLeft);

            var fileName = DateTime.Now.ToString("s").Replace(":", "_") + @".xlsx";
            var fileResult = resultFilesFolder + "ОстатокВблоках_" + fileName;
            ExcelGenerator.Generate(blocksLeftList, fileResult);

            byte[] fileBytes = System.IO.File.ReadAllBytes(fileResult);
            var responce = new FileContentResult(fileBytes, "application/octet-stream");
            responce.FileDownloadName = fileName;

            return responce;
        }
    }

    public class BlocksLeftModel
    {
        public HttpPostedFileBase ReportBlocksLeftFromAa { get; set; }
        public HttpPostedFileBase ReportChecksFromAa { get; set; }
        public HttpPostedFileBase ReportPhonesFromProfile { get; set; }
    }
}