﻿using System;
using System.Configuration;
using System.Web.Mvc;

namespace RepGen.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.BlocksLeftEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["BlocksLeftEnabled"]);
            ViewBag.SalesAfterStartEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["SalesAfterStartEnabled"]);
            ViewBag.RetentionEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["RetentionEnabled"]);
            ViewBag.VisitsAndSeanses = Convert.ToBoolean(ConfigurationManager.AppSettings["VisitsAndSeanses"]);

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}